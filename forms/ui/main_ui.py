# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created: Fri Jun 24 21:18:31 2016
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(332, 280)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.port = QtGui.QLineEdit(self.centralwidget)
        self.port.setGeometry(QtCore.QRect(70, 10, 113, 31))
        self.port.setObjectName(_fromUtf8("port"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(30, 10, 31, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.lbl_temp = QtGui.QLabel(self.centralwidget)
        self.lbl_temp.setGeometry(QtCore.QRect(20, 70, 231, 101))
        font = QtGui.QFont()
        font.setPointSize(48)
        font.setBold(True)
        font.setWeight(75)
        self.lbl_temp.setFont(font)
        self.lbl_temp.setObjectName(_fromUtf8("lbl_temp"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(260, 90, 64, 61))
        font = QtGui.QFont()
        font.setPointSize(48)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.btn_connect = QtGui.QPushButton(self.centralwidget)
        self.btn_connect.setGeometry(QtCore.QRect(190, 10, 95, 31))
        self.btn_connect.setObjectName(_fromUtf8("btn_connect"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 332, 27))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.port.setText(_translate("MainWindow", "/dev/ttyUSB0", None))
        self.label.setText(_translate("MainWindow", "Port:", None))
        self.lbl_temp.setText(_translate("MainWindow", "100,00", None))
        self.label_3.setText(_translate("MainWindow", "C", None))
        self.btn_connect.setText(_translate("MainWindow", "Connect", None))

