from PyQt4 import QtGui
import serial

from forms.code.thread_read_temp import thread_read_temp
from forms.ui.main_ui import Ui_MainWindow


class main_ui(QtGui.QMainWindow):

    def __init__(self):
        QtGui.QMainWindow.__init__(self)

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.btn_connect.clicked.connect(self.conectar)

        self.Port = None

        self.Th = thread_read_temp(None, self.ui.lbl_temp)
        self.Th.start()

        #self.BT = serial.Serial(port='/dev/tty.TEATRO', baudrate=9600)

    def conectar(self):
        if self.ui.btn_connect.text() == "Connect":
            print("Puerto: " + self.ui.port.text().__str__())
            self.Port = serial.Serial(port=self.ui.port.text().__str__(), baudrate=9600)
            self.Th.setPort(self.Port)
            self.Th.resume()
            self.ui.btn_connect.setText("Disconnect")
        else:
            self.Th.stop()
            self.ui.btn_connect.setText("Connect")