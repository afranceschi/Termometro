from threading import Thread
from time import sleep

import serial


class thread_read_temp(Thread):

    def __init__(self, SerialPort=serial.Serial(), uiLabel=None):
        Thread.__init__(self)
        self.stopFlag = True
        self.SP = SerialPort
        self.UiLabel = uiLabel

    def run(self):
        while True:
            while self.stopFlag:
                pass
            while not self.stopFlag:
                data = float(0.00)
                for i in range(50):
                    self.SP.write('T')
                    sleep(0.1)
                    try:
                        data += float(self.SP.read_all())
                    except:
                        data += float(0.00)
                data /= 50
                self.UiLabel.setText(str(round(data, 2)))
                #print("Data: " + data)
                #sleep(1)
            print("Fin de Thread")

    def stop(self):
        self.stopFlag = True

    def resume(self):
        self.stopFlag = False

    def setPort(self, serialPort):
        self.SP = serialPort