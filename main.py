import sys
from PyQt4 import QtGui
from forms.code.main import main_ui

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    print sys.platform
    f = main_ui()
    f.show()
    sys.exit(app.exec_())